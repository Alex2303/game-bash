#!/bin/bash

level=1
Link_HPMAX=60
Link_HP=$Link_HPMAX
Link_STR=10
Bokoblin_HPMAX=30
Bokoblin_STR=5
Bokoblin_HP=$Bokoblin_HPMAX
Bokoblin_damages=$(($Link_HP-$Bokoblin_STR))
Link_damages=$(($Bokoblin_HP-$Link_STR))
Ganon_HP=150
Ganon_STR=20
Ganon_damages=$(($Link_HP-$Ganon_STR))

while [ $level -lt 3 ]
do
    Bokoblin_HP=30
    while [ $Link_HP -gt 0 ] && [ $Bokoblin_HP -gt 0 ]
    do
	echo " *****Fight***** "
	echo " *****Round: $level "
	echo " You encounter a Bokoblin "
	echo " Bokoblin HP: 30/30 "
	echo " Link HP: 60/60"
	select item in "Attack" "Heal"
	do
	    if [ $item == "Attack" ]
	    then
		Bokoblin_HP=$(($Bokoblin_HP-$Link_STR))
		echo " You attacked and dealt $Link_STR damages the the Bokoblin "
		echo " Bokoblin HP: $Bokoblin_HP "
	    fi
	    if [ $Bokoblin_HP -le 0 ]
	    then
		break
	    fi
	    if [ $item == "Heal" ]
	    then
		echo "Heal"
		if [ $Link_HP -ge $(($Link_HPMAX/2)) ]
		then
		    echo " You cannot yet use the heal option because you're HP is up to 30"
		elif [ $Link_HP -le $(($Link_HPMAX/2)) ]
		then
		    Link_HP=$(($Link_HP+$Link_HPMAX/2))
		    echo " you received 30 HP "
		    echo "Link HP: $Link_HP" 
		fi
	    fi
	    if [ $Bokoblin_HP -gt 0 ]
	    then
		Link_HP=$(($Link_HP-$Bokoblin_STR))
		echo " The Bokoblin attacked you and deatl you $Bokoblin_STR damages  "
		echo " Link HP: $Link_HP "
	    fi
	    if [ $Link_HP -le 0 ]
	    then
		echo "Game Over"
		exit
		echo " You have loose this battle!"
	    fi
	done
    done
    Level=$(($Level+1))    
    while [ $Level -eq 3 ]
    do
	while [ $Link_HP -gt 0 ] && [ $Ganon_HP -gt 0 ]
	do
	    echo " *****Final round "
	    echo " You encounter the Boss "
	    echo " Ganon HP: 150/150 "
	    echo " Link HP: $Link_HP/60"
	    select item in "Attack" "Heal"
	    do
		if [ $item == "Attack" ]
		then
		    Ganon_HP=$(($Ganon_HP-$Link_STR))
		    echo " You attacked and dealt $Link_STR damages the Ganon "
		    echo " Ganon HP: $Ganon_HP "
		fi
		if [ $Ganon_HP -le 0 ]
		then
		    echo "***Congratulations! You WIN!*** "
		    exit
		fi
		if [ $item == "Heal" ]
		then
		    echo "Heal"
		    if [ $Link_HP -ge $(($Link_HPMAX/2)) ]
		    then
			echo " You cannot yet use the heal option because you're up to 30 HP"
		    elif [ $Link_HP -le $(($Link_HPMAX/2)) ]
		    then
			Link_HP=$(($Link_HP+$Link_HPMAX/2))
			echo " you received 30 HP "
			echo "Link HP: $Link_HP"
		    fi
		fi
		if [ $Ganon_HP -gt 0 ]
		then
		    Link_HP=$(($Link_HP-$Ganon_STR))
		    echo " The Ganon attacked you and deatl you $Ganon_STR damages  "
	            echo " Link HP: $Link_HP "
		    if [ $Link_HP -le 0 ]
                    then
			echo "Game Over"
			exit
	            fi
		fi
	    done
	done
    done
done
